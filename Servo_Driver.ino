/*****************************************************
* This program uses an Wattduino board for controlling
* servos. The protocoll used is the same for SSC-32 
* from Lynxmotion
* http://www.lynxmotion.com/images/html/build136.htm
*****************************************************/

#include "define.h"
#include "string.h"
#include "stdlib.h"
#include "Servo.h"

/*****************************
* Data storage from the Serial
* port about servos
*
* Time values are in: 
* [microseconds]
*
****************************/
struct servoData {
  uint8_t ch;       // number of the servo
  uint16_t pw;      // pulse width in microseconds []
  uint16_t spd;     // movement speed in uS per second for one channel
  uint16_t time;    // time in mSeconds for the entire move (for all channel)
  int offset;       // it takes between 100 to -100 in uSeconds
};

servoData servoArray[FIFO_SIZE];

/***************
* Servo creation
*
****************/
Servo channel[ FIFO_SIZE ];

/***************************************************
* Definition of the SAMPLE TIME
* for reading the sensors
*
* Because it could be adjusted in different
* version of the program, then is is going 
* to be saved in a variable.
*
* change the value if you need another sampling time
* DAFAULT VALUE 20 Milliseconds = 0,02 seconds
***************************************************/ 
uint8_t deltaT = 1000;  // 20 milliseconds
uint8_t ledDeltaT = 500; // all 0.5 seconds  
uint16_t elapsedTimeSlow = 0;
uint16_t elapsedTimeFast = 0;

/***********************
* Led for status display
***********************/
const uint8_t ledPin = 13;

/***********************
* For reading the String
* in the serial port
***********************/
char serialBuffer[ BUFFER_SIZE ];
uint8_t bufferCount = 0;
char *strgEnd, *serialBufferPointer;

const char delimiter1 = '#';
const char delimiter2 = 'P';
const char delimiter3 = 'S';
const char delimiter4 = 'T';
const char delimiter5 = 'O';

// keep track of the last servo through the serial port selected
int counterServo2Update = 0;

//###########################################################################################
/***********************************
* Definitions of function prototypes
***********************************/
void evalSerialData( void );
long int extractNumber( const char *s );
//###########################################################################################

void setup() {

	delay( 2000 );

	Serial.begin( BAUDRATE );
	Serial.println( "Servo Driver - Wattduino" );

	delay( 2000 );

    pinMode( ledPin, OUTPUT );

    // servo initialization and pinMode definition
    for( int j = 0; j < FIFO_SIZE; j++ ) {
        pinMode(j + 2, OUTPUT);    // plus 2 because on Wattduino there is no digital 0 or digital 1
        channel[j].attach(j + 2);
    }

    // initialize all servos to a neutral position
    for( uint8_t i = 0; i < FIFO_SIZE; i++ ) {
        servoArray[i].ch = i + 1;
        servoArray[i].pw = ( MIN_PULSE_SERVO + MAX_PULSE_SERVO ) / 2;
        servoArray[i].spd = 0;
        servoArray[i].time = 0;
        servoArray[i].offset = 0;
    }

}

void loop() {

    uint16_t currentTime = millis();

    if( currentTime - elapsedTimeFast > deltaT ) {
        elapsedTimeFast = currentTime;

        for( uint8_t i = 0; i < FIFO_SIZE; i++ ) {

            // channel[i].writeMicroseconds( servoArray[i].pw );
            Serial.print( servoArray[i].ch );
            Serial.print( ',' );
            Serial.print( servoArray[i].pw );
            Serial.print( ',' );
            Serial.print( servoArray[i].spd );
            Serial.print( ',' );
            Serial.print( servoArray[i].time );
            // Serial.print( ',' );
            // Serial.print( servoArray[i].offset );
            Serial.println();
        }
    }

    if( currentTime - elapsedTimeSlow > ledDeltaT ) {
        elapsedTimeSlow = currentTime;

        digitalWrite( ledPin, !digitalRead( ledPin ) );
    }

}


void serialEvent() {

    char msg = Serial.read();

    serialBuffer[ bufferCount ] = msg;
    // check if the buffer overflows, if yes trunc the buffer
    // so it prevents to overflow in the case the '\0' is not received
    (bufferCount <= BUFFER_SIZE ) ? bufferCount++ : msg = 13;

    if( msg == 13 ) {
        evalSerialData();
    } 
}


void evalSerialData( void ) {
    
    long int numValue;

    // serialBuffer has the message from the serial port
    // now point a pointer to the buffer itself
    serialBufferPointer = strchr( serialBuffer, delimiter1 );

    if( serialBufferPointer != NULL ) {        // if the first char found is '#':
        numValue = (int) extractNumber( serialBufferPointer );
        if( numValue <= FIFO_SIZE ) {     // check if the number is smaller than available servo to be used
            counterServo2Update = numValue;            // use this keep track of which servo is going to be updated
            servoArray[ counterServo2Update - 1 ].ch = numValue;  // 1 must be subtracted because the array starts from 0 for CH1    
            
            // now check for the second delimiter if present
            serialBufferPointer = strchr( serialBuffer, delimiter2 ); 

            if( serialBufferPointer != NULL ) {        // if the char for delimiter2 is present, dann update that field
                numValue = (uint16_t) extractNumber( serialBufferPointer );
                if( ( numValue > MIN_PULSE_SERVO ) && ( numValue < MAX_PULSE_SERVO ) ) {     // check if the number between the range
                    servoArray[ counterServo2Update - 1 ].pw = numValue;  // 1 must be subtracted because the array starts from 0 for CH1, 1 for CH2 and so on
                }
            }

            // now check for the third delimiter if present
            serialBufferPointer = strchr( serialBuffer, delimiter3 ); 

            if( serialBufferPointer != NULL ) {        // if the char for delimiter3 is present, dann update that field
                numValue = (uint16_t) extractNumber( serialBufferPointer );
                if( ( numValue > MIN_SPEED_SERVO ) && ( numValue < MAX_SPEED_SERVO ) ) {     // check if the number between the range
                    servoArray[ counterServo2Update - 1 ].spd = numValue;  // 1 must be subtracted because the array starts from 0 for CH1, 1 for CH2 and so on
                }
            }

            // now check for the fourth delimiter if present
            serialBufferPointer = strchr( serialBuffer, delimiter4 ); 

            if( serialBufferPointer != NULL ) {        // if the char for delimiter3 is present, dann update that field
                numValue = (uint16_t) extractNumber( serialBufferPointer );
                if( ( numValue > MIN_TIME_SERVO ) && ( numValue < MAX_TIME_SERVO ) ) {     // check if the number between the range
                    servoArray[ counterServo2Update - 1 ].time = numValue;  // 1 must be subtracted because the array starts from 0 for CH1, 1 for CH2 and so on
                }
            }

        }



    }

    // the counter must be reset on exit otherwise no other charachter are going to be read in the buffer
    bufferCount = 0;

}


long int extractNumber( const char *s ) {

    char buff[ MICRO_BUFFER_SIZE ];
    uint8_t buffCount = 0;

    // it uses the pointer arithmetics to read the number beyond the first symbol
    while( isdigit( *( s + 1 + buffCount ) ) && ( buffCount < MICRO_BUFFER_SIZE ) ) {  // check if the next symbol is a number or character
        buff[ buffCount ] = *( s + 1 + buffCount );
        buffCount++;
    } // exit otherwise and close the micro buffer

    buff[ buffCount ] = '\0';

    return strtol( buff, &strgEnd, 0 );
}