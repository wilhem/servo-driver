/*****************************************************
* General definition for general labels which are used 
* everywhere in the code
*****************************************************/

#ifndef define_h
#define define_h

#define BAUDRATE 115200

#define BUFFER_SIZE 30   // this stores the msg from PC or another Arduino sent by serial
#define MICRO_BUFFER_SIZE 8 // used for the small buffer in the fuction extractNumber

// change this one to reduce or add more servos to drive
#define FIFO_SIZE 1  // it is also for the whole number of servos     
                         
#define MIN_PULSE_SERVO 300
#define MAX_PULSE_SERVO 2500

#define MIN_SPEED_SERVO 0
#define MAX_SPEED_SERVO 360

#define MIN_TIME_SERVO 0
#define MAX_TIME_SERVO 10000

#endif